package frc.robot.subsystems;

import com.kauailabs.navx.frc.*;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.*;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class differentialDrive {
    private MotorController frontLeft, backLeft, frontRight, backRight;
    private MotorControllerGroup left, right;
    public DifferentialDrive m_differentialDrive;
    public double pidDistance;

    public static final class pointDriveConstants {
        public static final double kP = 1 / 90;
        public static final double kI = 0.00;
        public static final double kD = 0.00;
        public static final double kTurnSpeed = 1;
    }

    public differentialDrive() {
        frontLeft = new CANSparkMax(0, MotorType.kBrushless);
        backLeft = new VictorSP(1);
        frontRight = new CANSparkMax(2, MotorType.kBrushless);
        backRight = new VictorSP(3);

        left = new MotorControllerGroup(frontLeft, backLeft);
        right = new MotorControllerGroup(frontRight, backRight);

        m_differentialDrive = new DifferentialDrive(left, right);
    }

    public void arcadeDrive(double y, double theta) {
        m_differentialDrive.arcadeDrive(y, theta);
    }

    public void curveDrive(double y, double theta, boolean turnInPlace) {
        m_differentialDrive.curvatureDrive(y, theta, turnInPlace);
    }

    public void tankDrive(double left, double right) {
        m_differentialDrive.tankDrive(left, right);
    }

    public void pointDrive(PIDController pid, double x, double y, AHRS ahrs, double moveSpeedMultiplyer,
            double turnSpeedBounds) {
        // remember to set the origin point of your navx beforehand

        // get convert the x and y values into degrees
        double direction = Math.atan2(x, y);
        direction = Units.radiansToDegrees(direction);

        // get distance from center
        double magnitude = (Math.abs(x) + Math.abs(y)) / 2;

        // get the heading on the navx
        double heading = ahrs.getYaw();

        // set the distance to cover for readout
        this.pidDistance = Math.abs(Math.abs(direction) - Math.abs(heading));

        // config pid
        pid.setPID(pointDriveConstants.kP, pointDriveConstants.kI, pointDriveConstants.kD);

        // calculate pid to turn the correct amount
        double rotation = pid.calculate(heading, direction);

        // clamp the pid to the currect amount;
        rotation = MathUtil.clamp(rotation, -turnSpeedBounds, turnSpeedBounds);

        // calculate speed
        double speed = magnitude * moveSpeedMultiplyer;

        m_differentialDrive.arcadeDrive(speed, rotation);
    }

    public void stopDrive() {
        m_differentialDrive.stopMotor();
    }

}
