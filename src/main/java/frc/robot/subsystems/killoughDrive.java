package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.drive.KilloughDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public class killoughDrive {
    private MotorController leftMotor, rightMotor, backMotor;
    public double moveSpeed, turnSpeed;
    public KilloughDrive m_killoughDrive;

    public killoughDrive() {
        leftMotor = new CANSparkMax(0, MotorType.kBrushless);
        rightMotor = new CANSparkMax(1, MotorType.kBrushless);
        backMotor = new CANSparkMax(2, MotorType.kBrushless);

        m_killoughDrive = new KilloughDrive(leftMotor, rightMotor, backMotor);
    }

    public void driveCartesian(double x, double y, double theta) {
        driveCartesian(y, x, theta, 0.0);
    }

    public void driveCartesian(double x, double y, double theta, double gyroAngle) {
        if (moveSpeed != 0.0 & turnSpeed != 0.0) {
            x = x * moveSpeed;
            y = y * moveSpeed;
            theta = theta * turnSpeed;
        }
        m_killoughDrive.driveCartesian(y, x, theta, gyroAngle);
    }

    public void drivePolar(double angle, double magnitude, double theta) {
        m_killoughDrive.drivePolar(magnitude, angle, theta);
    }

    public void setSpeed(double moveSpeed, double turnSpeed) {
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
    }

    public void stopDrive() {
        m_killoughDrive.stopMotor();
    }
}
