package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public class mecanumDrive {
    private MotorController frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor;
    public double moveSpeed, turnSpeed;
    public MecanumDrive m_mecanumDrive;

    public mecanumDrive() {
        frontLeftMotor = new CANSparkMax(0, MotorType.kBrushless);
        rearLeftMotor = new CANSparkMax(1, MotorType.kBrushless);
        frontRightMotor = new CANSparkMax(2, MotorType.kBrushless);
        rearRightMotor = new CANSparkMax(3, MotorType.kBrushless);

        m_mecanumDrive = new MecanumDrive(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
    }

    public void driveCartesian(double x, double y, double theta) {
        driveCartesian(y, x, theta, 0.0);
    }

    public void driveCartesian(double x, double y, double theta, double gyroAngle) {
        if (moveSpeed != 0.0 & turnSpeed != 0.0) {
            x = x * moveSpeed;
            y = y * moveSpeed;
            theta = theta * turnSpeed;
        }
        m_mecanumDrive.driveCartesian(y, x, theta, gyroAngle);
    }

    public void drivePolar(double angle, double magnitude, double theta) {
        m_mecanumDrive.drivePolar(magnitude, angle, theta);
    }

    public void setSpeed(double moveSpeed, double turnSpeed) {
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
    }

    public void stopDrive() {
        m_mecanumDrive.stopMotor();
    }
}
