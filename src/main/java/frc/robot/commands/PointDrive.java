package frc.robot.commands;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.subsystems.differentialDrive;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class PointDrive extends CommandBase {
    differentialDrive m_differentialDrive;
    XboxController controller;
    AHRS ahrs;
    PIDController pid;

    public PointDrive(differentialDrive m_differentialDrive) {
        this.m_differentialDrive = m_differentialDrive;
        this.controller = new XboxController(0);
        this.ahrs = new AHRS(SPI.Port.kMXP);
        this.pid = new PIDController(0.03, 0, 0);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void execute() {

        m_differentialDrive.pointDrive(pid, controller.getLeftX(), controller.getLeftY(), ahrs, 0.75, 0.75);

    }

    @Override
    public void end(boolean inturrupted) {

    }
}
