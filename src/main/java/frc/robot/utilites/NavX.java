package frc.robot.utilites;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;

public class NavX {
    AHRS ahrs;

    public void setNavX() {
        ahrs = new AHRS(SPI.Port.kMXP);
    }

    public boolean isNavXConnected() {
        return ahrs.isConnected();
    }

    /** @return Returns Pitch bound between -180, 180 */
    public float getPitch() {
        return ahrs.getPitch();
    }

    /** @return Returns Yaw bound between -180, 180 */
    public float getYaw() {
        return ahrs.getYaw();
    }

    /** @return Returns Roll bound between -180, 180 */
    public float getRoll() {
        return ahrs.getRoll();
    }

    public void setYaw() {
        ahrs.zeroYaw();
    }
}
