package frc.robot.utilites;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public class MotorControl {
    public void setMotorSpeed(double speed, MotorController... motors) {
        for (MotorController motor : motors) {
            motor.set(speed);
        }
    }

    public void stopMotor(MotorController... motors) {
        for (MotorController motor : motors) {
            motor.stopMotor();
        }
    }

}
