package frc.robot.utilites;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;

public class PneumaticControl {

    public void toggleSolenoid(Solenoid... solenoids) {
        for (Solenoid solenoid : solenoids) {
            solenoid.toggle();
        }
    }

    public void timedSolenoid(double duration, Solenoid... solenoids) {
        Timer m_timer = new Timer();
        m_timer.start();
        for (Solenoid solenoid : solenoids) {
            solenoid.set(true);
        }
        if (m_timer.hasElapsed(duration)) {
            m_timer.stop();
            for (Solenoid solenoid : solenoids) {
                solenoid.set(false);
            }
        }
    }

}
