package frc.robot.utilites;

import edu.wpi.first.math.MathUtil;

public class MathTools {
    public double randomMathDouble() {
        return randomMathDouble(0, 1);
    }

    public double randomMathDouble(double upperBound) {
        return randomMathDouble(0, upperBound);
    }

    public double randomMathDouble(double lowerBound, double upperBound) {
        double range = upperBound - lowerBound;
        return (Math.random() * range + lowerBound);
    }

    public double randomMathInt() {
        return randomMathInt(0, 1);
    }

    public double randomMathInt(int upperBound) {
        return randomMathInt(0, upperBound);
    }

    public double randomMathInt(int lowerBound, int upperBound) {
        int range = upperBound - lowerBound;
        double random = (Math.random() * range) + lowerBound;
        return (int) (MathUtil.clamp(random, lowerBound, upperBound));
    }

    // returns values using math.random

    // could alternatively use a Random object from java util
    // random object using next and an upper bound(optional)
    // allows any random type to be returned
}
