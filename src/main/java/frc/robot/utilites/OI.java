package frc.robot.utilites;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

public class OI {

    double Triggerdeadband = 0.1;
    double JoystickDeadband = 0.1;

    public double getLeftJoystickAsDirection(XboxController controller) {
        return Math.atan2(-controller.getLeftY(), controller.getLeftX());
    }

    public double getRightJoystickAsDirection(XboxController controller) {
        return Math.atan2(-controller.getRightY(), controller.getRightX());
    }

    public double getLeftYAsDouble(XboxController controller) {
        return -controller.getLeftY();
    }

    public double getLeftXAsDouble(XboxController controller) {
        return controller.getLeftX();
    }

    public double getRightYAsDouble(XboxController controller) {
        return -controller.getRightY();
    }

    public double getRightXAsDouble(XboxController controller) {
        return controller.getRightX();
    }

    public double getLTAsDouble(XboxController controller) {
        return controller.getLeftTriggerAxis();
    }

    public double getRTAsDouble(XboxController controller) {
        return controller.getRightTriggerAxis();
    }

    public JoystickButton getAAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kA.value);
    }

    public JoystickButton getBAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kB.value);
    }

    public JoystickButton getXAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kX.value);
    }

    public JoystickButton getYAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kY.value);
    }

    public JoystickButton getLBAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kLeftBumper.value);
    }

    public JoystickButton getRBAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kRightBumper.value);
    }

    public JoystickButton getLSAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kLeftStick.value);
    }

    public JoystickButton getRSAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kRightStick.value);
    }

    public JoystickButton getStartAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kStart.value);
    }

    public JoystickButton getBackAsButton(XboxController controller) {
        return new JoystickButton(controller, XboxController.Button.kStart.value);
    }

    public boolean getLSMovingAsBoolean(XboxController controller) {
        return (Math.abs(controller.getLeftY()) + Math.abs(controller.getLeftX())) > JoystickDeadband;
    }

    public boolean getRSMovingAsBoolean(XboxController controller) {
        return (Math.abs(controller.getRightY()) + Math.abs(controller.getRightX())) > JoystickDeadband;
    }

    public boolean getLTAsBoolean(XboxController controller) {
        return controller.getLeftTriggerAxis() > Triggerdeadband;
    }

    public boolean getRTAsBoolean(XboxController controller) {
        return controller.getRightTriggerAxis() > Triggerdeadband;
    }

    public boolean getAAsBoolean(XboxController controller) {
        return controller.getAButton();
    }

    public boolean getBAsBoolean(XboxController controller) {
        return controller.getBButton();
    }

    public boolean getXAsBoolean(XboxController controller) {
        return controller.getXButton();
    }

    public boolean getYAsBoolean(XboxController controller) {
        return controller.getYButton();
    }

    public boolean getLBAsBoolean(XboxController controller) {
        return controller.getLeftBumper();
    }

    public boolean getRBAsBoolean(XboxController controller) {
        return controller.getRightBumper();
    }

    public boolean getLSAsBoolean(XboxController controller) {
        return controller.getLeftStickButton();
    }

    public boolean getRSAsBoolean(XboxController controller) {
        return controller.getRightStickButton();
    }

    public boolean getStartAsBoolean(XboxController controller) {
        return controller.getStartButton();
    }

    public boolean getBackAsBoolean(XboxController controller) {
        return controller.getBackButton();
    }

}
